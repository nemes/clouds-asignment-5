import './App.css';
import {auth} from './utils/firebase'
import { useAuthState } from 'react-firebase-hooks/auth';
import { BrowserRouter, Routes, Route } from "react-router-dom";
import Home from './pages/Home';
import Login from './pages/Login';
import NavigationBar from './components/Navigationbar';
import Container from 'react-bootstrap/Container';
import Favorites from './pages/Favorites';
import { useEffect } from 'react';

function App() {

  const [user] = useAuthState(auth);

  return (
    <Container fluid>
      <div className="App">
        <BrowserRouter>
            <NavigationBar/>
            <Routes>
              <Route path="login" element={<Login user={user} />} />
              <Route path="" element={<Home user={user}/>} />
              <Route path="wishlist" element={<Favorites user={user} />} />
            </Routes>
        </BrowserRouter>
      </div>
    </Container>
  );
}

export default App;
