import { useNavigate } from "react-router-dom";
import {auth} from '../utils/firebase'
import { useAuthState } from 'react-firebase-hooks/auth';
import { logout } from "../utils/auth";
import Navbar from "react-bootstrap/Navbar";
import Button from "react-bootstrap/Button"
import Nav from "react-bootstrap/Nav"

function NavigationBar () {

  const [user] = useAuthState(auth);
  const navigate = useNavigate();

  return(<div>
      <Navbar bg="dark" variant="dark">
          <Nav className="container-fluid">
            <Navbar.Brand>
              <a href="#" onClick={() => {navigate('/')}}>
                <img
                  src="/favicon.ico"
                  width="30"
                  height="30"
                  className="d-inline-block align-top"
                  alt="React Bootstrap logo"
                />
              </a>
              <span className="ml-3">
                Movies room
              </span>
            </Navbar.Brand>
          </Nav>
          <Nav>
              {user ?
                (<Navbar.Brand>
                    {user.displayName}
                  </Navbar.Brand>)
                : (<span></span>)}
          </Nav>
          <Nav.Link className>
            {user ? 
              (<form className="d-flex">
                  <Button onClick={() =>{logout()}}>Logout</Button>
                </form>)
              : (<span></span>)}
          </Nav.Link>  
          <Nav.Link className>
            {user ? 
              (<form className="d-flex">
                  <Button onClick={() => {navigate('/wishlist')}}>Wishlist</Button>
                </form>)
              : (<span></span>)}  
          </Nav.Link>  
      </Navbar>
      <h1 className="mt-3">Welcome to <b>Movie Club</b>!</h1>
  </div>);
}

export default NavigationBar;
