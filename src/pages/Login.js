
import { useNavigate } from "react-router-dom";
import { googleHandler } from '../utils/auth';
import Button from 'react-bootstrap/Button';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import Image from 'react-bootstrap/Image'
import { useEffect } from 'react';

function Login ({user}) {
    const navigate = useNavigate();

    useEffect(() => {
        if (user) {
            if (window.location.href.endsWith('wishlist')) {
                navigate('/wishlist')
            } else {
                navigate('/')
            }
        }

    }, [user, navigate])


    return (
        <Container fluid>
            {/* {user 
                ? (<div>
                    {window.location.href.endsWith('wishlist') 
                        ? (<Navigate to="/wishlist"/>)
                        : (<Navigate to="/"/>)}
                    </div>) 
                : (<div></div>)} */}
            <Row md={6}>
                <Col md={3}>
                </Col>
                <Col md={6}>
                <Image className="mb-3 mt-3" src="popcorn-logo.jpg" />
                </Col>
                <Col md={3}>
                </Col>
            </Row>
            <Row md={6}>
                <Col md={3}>
                </Col>
                <Col md={6}>
                    <Button className="mt-3" onClick={() => {googleHandler()}}>Sign in using Google</Button>
                </Col>
                <Col md={3}>
                </Col>
            </Row>
        </Container>
    )
}

export default Login;
