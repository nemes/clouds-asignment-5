import { useEffect, useState } from 'react';
import { useNavigate } from "react-router-dom";
import { getAllMovies, getMyFavorites, removeFavorite } from '../utils/database';
import Table from 'react-bootstrap/Table'
import Spinner from 'react-bootstrap/Spinner'
import Button from '@restart/ui/esm/Button';
import { Col, Row } from 'react-bootstrap';
import Container from 'react-bootstrap/Container'

function Favorites ({user}) {

    const navigate = useNavigate();
    const [movies, setMovies] = useState([]);
    const [toDisplay, setToDisplay] = useState([])
    const [areRetrieved, setAreRetrieved] = useState(false);

    useEffect(async () =>{
        if (!user) {
            navigate('/login?r=wishlist')
        } else {
            let retrieved = getAllMovies();
            let favorites = getMyFavorites(user.uid);

            retrieved = await retrieved;
            favorites = await favorites;
            const favoriteMovies = retrieved.filter((movie) => favorites[movie.id]);

            setMovies(favoriteMovies);
            setToDisplay(favoriteMovies);
            setAreRetrieved(true);
        }
    }, [user])

    const stringifyMovie = (movie) => `${movie.genre} ${movie.title} ${movie.year}`.toLowerCase();

    const handleSearch = (e) => {
        e.preventDefault();
        const searchTerm = e.target[0].value.toLowerCase();
        setToDisplay(
            movies.filter(movie => stringifyMovie(movie).includes(searchTerm))
        );
    }

    const onUnFavorite = (movieId) => {
        removeFavorite(user.uid, movieId);
        const filteredAll = movies.filter((movie) => movie.id != movieId);
        const filteredDisplay = toDisplay.filter((movie) => movie.id != movieId);
        setMovies(filteredAll);
        setToDisplay(filteredDisplay);
    }

    return (
        <div>
            {(movies.length == 0 || !areRetrieved) ? (
                <div>
                    {areRetrieved 
                    ? (<div>
                        <h2>There are no movies in your wishlist!</h2>
                        <Button onClick={() => {navigate('/')}}>Go to movies list</Button>
                      </div>)
                    : (<div>
                        <h2>Retrieving wishlist, hold on!</h2>
                        <Spinner animation="border" role="status">
                            <span className="visually-hidden">Loading...</span>
                        </Spinner>
                      </div>)
                    }
                </div>
            ) : (
            <div>
                <Container fluid>
                    <Row>
                        <Col md={3}>
                        </Col>
                        <Col md={6}>
                            <form onSubmit={handleSearch}>
                                <input name="searchQuery"
                                    className="mt-3 mb-3 mr-3" type="text"/>
                                <Button className="ml-3" type="submit">Search</Button>
                            </form>
                        </Col>
                        <Col md={3}>
                        </Col>
                    </Row>
                </Container>
                <Table bordered hover>
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Title</th>
                            <th>Year</th>
                            <th>Genre</th>
                            <th>Unfavorite</th>
                        </tr>
                    </thead>
                    <tbody>
                        {toDisplay.map((movie) => {return(<tr key={movie.id}>
                                <td>{movie.id}</td>
                                <td>{movie.title}</td>
                                <td>{movie.year}</td>
                                <td>{movie.genre}</td>
                                <td><Button onClick={() => {onUnFavorite(movie.id)}}>Unfavorite</Button></td>
                            </tr>)})}
                    </tbody>
                
                </Table>
            </div>
            )}
        </div>
    )
}

export default Favorites;
