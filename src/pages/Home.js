import { useEffect, useState } from 'react';
import { useNavigate } from "react-router-dom";
import { addFavorite, getAllMovies, getMyFavorites } from '../utils/database';
import Table from 'react-bootstrap/Table'
import Container from 'react-bootstrap/Container'
import Spinner from 'react-bootstrap/Spinner'
import Button from '@restart/ui/esm/Button';
import { Col, Row } from 'react-bootstrap';

function Home ({user}) {

    const [movies, setMovies] = useState([]);
    const [toDisplay, setToDisplay] = useState([])
    const navigate = useNavigate();

    useEffect(async () =>{
        if (!user) {
            navigate('/login?r=dashboard')
        } else {
            let retrieved = getAllMovies();
            let favorites = getMyFavorites(user.uid);

            retrieved = await retrieved;
            favorites = await favorites;

            const nonFavorites = retrieved.filter((movie) => !(favorites[movie.id]));
            
            setMovies(nonFavorites);
            setToDisplay(nonFavorites);
        }
    }, [user])

    const stringifyMovie = (movie) => `${movie.genre} ${movie.title} ${movie.year}`.toLowerCase();

    const handleSearch = (e) => {
        e.preventDefault();
        const searchTerm = e.target[0].value.toLowerCase();
        setToDisplay(
            movies.filter(movie => stringifyMovie(movie).includes(searchTerm))
        );
    }

    const onFavorite = (movieId) => {
        addFavorite(user.uid, movieId);
        const filteredAll = movies.filter((movie) => movie.id != movieId);
        const filteredDisplay = toDisplay.filter((movie) => movie.id != movieId);
        setMovies(filteredAll);
        setToDisplay(filteredDisplay);
    }

    return (
        <div>
            <Container fluid>
                <Row>
                    <Col md={3}>
                    </Col>
                    <Col md={6}>
                        <form onSubmit={handleSearch}>
                            <input name="searchQuery"
                                className="mt-3 mb-3 mr-3" type="text"/>
                            <Button className="ml-3" type="submit">Search</Button>
                        </form>
                    </Col>
                    <Col md={3}>
                    </Col>
                </Row>
            </Container>
            {movies.length == 0 ? (
                <div>
                    <h2>Hold on, loading movie list</h2>
                    <Spinner animation="border" role="status">
                        <span className="visually-hidden">Loading...</span>
                    </Spinner>
                </div>
            ) : (
            <Table bordered hover>
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Title</th>
                        <th>Year</th>
                        <th>Genre</th>
                        <th>Favorite</th>
                    </tr>
                </thead>
                <tbody>
                    {toDisplay.map((movie) => {return(<tr key={movie.id}>
                            <td>{movie.id}</td>
                            <td>{movie.title}</td>
                            <td>{movie.year}</td>
                            <td>{movie.genre}</td>
                            <td><Button onClick={() => {onFavorite(movie.id)}}>Favorite</Button></td>
                        </tr>)})}
                </tbody>
            
            </Table>
            )}
        </div>
    )
}

export default Home;
