import { signInWithPopup, GoogleAuthProvider } from 'firebase/auth';
import { auth, provider } from './firebase'; // update path to your firestore config
import { signOut } from 'firebase/auth';
import { saveOnFirstLogin as saveOnFirstLogin } from './database';

export const googleHandler = async () => {
        provider.setCustomParameters({ prompt: 'select_account' });
        const result = await signInWithPopup(auth, provider)
        const user = result.user;
        await saveOnFirstLogin(user.email, user.displayName, user.uid);
    };


export const logout = () => {
    signOut(auth)
}