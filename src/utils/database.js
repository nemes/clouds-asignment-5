import { ref, set, child, get, remove } from "firebase/database";
import { collection, doc, setDoc, getDoc } from "firebase/firestore"; 
import { db, fireStoreDb } from './firebase'

export const saveOnFirstLogin = async (email, name, uid) => {
    const docRef = doc(fireStoreDb, "users", uid);
    const docSnap = await getDoc(docRef);

    if (!docSnap.exists()) {
        const usersRef = collection(fireStoreDb, "users");
        await setDoc(doc(usersRef, uid), {
            name: name,
            email: email,
            favorites: {}
        });
    }
}

export const addFavorite = async (uid, index) => {
    const docRef = doc(fireStoreDb, "users", uid);
    const docSnap = await getDoc(docRef);
    const user = docSnap.data();

    const favorites = {...user.favorites};
    favorites[index] = true;

    const usersRef = collection(fireStoreDb, "users");
    await setDoc(doc(usersRef, uid), {
        ...user,
        favorites
    })
}

export const removeFavorite = async (uid, index) => {
    const docRef = doc(fireStoreDb, "users", uid);
    const docSnap = await getDoc(docRef);
    const user = docSnap.data();

    let favorites = {...user.favorites};
    delete favorites[index]

    const usersRef = collection(fireStoreDb, "users");
    await setDoc(doc(usersRef, uid), {
        ...user,
        favorites
    })
}

const waitFor = delay => new Promise(resolve => setTimeout(resolve, delay));

export const getMyFavorites = async (uid) => {
    const docRef = doc(fireStoreDb, "users", uid);
    let docSnap = await getDoc(docRef);
    let result = null;
    if (!docSnap.exists()) {
        //if it's the first time the user is logging in
        //there is no guarantee that they have been saved on time
        //retry retriving it after short delay
        await waitFor(2000);
        docSnap = await getDoc(docRef);
    }
    result = docSnap.data();
    return result.favorites;
}

export const getAllMovies = async (callback1, callback2) => {
    const dbRef = ref(db);
    try {
        const snapshot = await get(child(dbRef, `movies-list`));
        return snapshot.val();
    } catch (error) {
        alert('Failed in retrieving movies, please refresh page')
    }
    return []
}