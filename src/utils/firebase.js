import { initializeApp } from 'firebase/app';
import { getAuth, GoogleAuthProvider } from 'firebase/auth';
import { getDatabase } from "firebase/database";
import {getFirestore} from "firebase/firestore"

const firebaseConfig = {
  apiKey: "AIzaSyBALsLOx5ZZE1DK8IPesTcTv5F-uMlthnM",
  authDomain: "asignment-5-3bf53.firebaseapp.com",
  databaseURL: "https://asignment-5-3bf53-default-rtdb.europe-west1.firebasedatabase.app",
  projectId: "asignment-5-3bf53",
  storageBucket: "asignment-5-3bf53.appspot.com",
  messagingSenderId: "309540423955",
  appId: "1:309540423955:web:188de67afa34148da7b2e6",
  measurementId: "G-7JSF3H0R73"
};

const app = initializeApp(firebaseConfig);

export const db = getDatabase(app);
export const fireStoreDb = getFirestore(app);

export const auth = getAuth();
export const provider = new GoogleAuthProvider();
provider.setCustomParameters({ prompt: 'select_account' });
